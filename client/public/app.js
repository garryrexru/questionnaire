import 'regenerator-runtime/runtime';
import axios from 'axios';


const BASE_URL = 'http://127.0.0.1:8000';


var vote = document.getElementById("vote")


var question_form_counter = 0;
vote.addEventListener('click', event => {
    if (question_form_counter < 1) {
        create_question_form()
        question_form_counter++
    }
})


function create_question_form() {

    var div = document.createElement("div");
    var h1 = document.createElement("h1");
    var input = document.createElement("input");
    var button = document.createElement("button");

    h1.appendChild(document.createTextNode("Введите тему голосования"));
    button.appendChild(document.createTextNode("Готово"));

    input.setAttribute("type", "text");
    input.setAttribute("id", "new_answer__title");
    button.setAttribute("type", "submit");
    button.setAttribute("id", "question_button");

    div.appendChild(h1);
    div.appendChild(input);
    div.appendChild(button);

    document.body.appendChild(div);
}

var question_button = document.getElementById("question_button")


var answer_form_counter = 0;
question_button.addEventListener('click', event => {
    if (answer_form_counter < 4) {
        create_answer_form()
        answer_form_counter++
    }
})


function create_answer_form() {

    var div = document.createElement("div");
    var h1 = document.createElement("h1");
    var input = document.createElement("input");

    h1.appendChild(document.createTextNode("Введите вопрос"));

    input.setAttribute("type", "text");
    input.setAttribute("id", "new_answer__title");

    div.appendChild(h1);
    div.appendChild(input);

    document.body.appendChild(div);
}