""" Модели опроса."""

import datetime

from pydantic import BaseModel
from typing import Optional

__all__ = ["Poll", "PollEntry", "PollUpdate", "PollDelete"]


class Poll(BaseModel):
    id: int
    name: str
    date: Optional[datetime.datetime]
    type: int
    disable: bool


class PollEntry(BaseModel):
    name: str
    type: int
    disable: bool


class PollUpdate(BaseModel):
    id: int
    name: str
    type: int
    disable: bool


class PollDelete(BaseModel):
    id: int
