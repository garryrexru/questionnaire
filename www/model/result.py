""" Модель результата."""


from pydantic import BaseModel


__all__ = ["Result", "ResultEntry"]


class Result(BaseModel):
    id: int
    question_id: int
    answer_id: int


class ResultEntry(BaseModel):
    question_id: int
    answer_id: int
