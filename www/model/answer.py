""" Модель ответа."""


from pydantic import BaseModel


__all__ = ["Answer", "AnswerEntry", "AnswerUpdate", "AnswerDelete"]


class Answer(BaseModel):
    id: int
    title: str
    question_id: int


class AnswerEntry(BaseModel):
    title: str
    question_id: int


class AnswerUpdate(BaseModel):
    id: int
    title: str
    question_id: int


class AnswerDelete(BaseModel):
    id: int
