""" Модели пользователя."""

import datetime

from pydantic import BaseModel
from typing import Optional

__all__ = ["User", "UserEntry", "UserUpdate", "UserDelete"]


class User(BaseModel):
    id: int
    username: str
    password: str
    first_name: str
    last_name: str
    create_at: Optional[datetime.datetime]


class UserEntry(BaseModel):
    username:   str
    password:   str
    first_name: str
    last_name: str


class UserUpdate(BaseModel):
    id: int
    first_name: str
    last_name: str


class UserDelete(BaseModel):
    id: int
