""" Модель вопроса."""


from pydantic import BaseModel


__all__ = ["Question", "QuestionEntry", "QuestionUpdate", "QuestionDelete"]


class Question(BaseModel):
    id: int
    title: str
    poll_id: int
    free_answer: bool


class QuestionEntry(BaseModel):
    title: str
    poll_id: int
    free_answer: bool


class QuestionUpdate(BaseModel):
    id: int
    title: str
    poll_id: int
    free_answer: bool


class QuestionDelete(BaseModel):
    id: int
