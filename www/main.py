""" Точка входа."""

import uvicorn

from fastapi import FastAPI

from www.core.db import database

__all__ = ["app"]

if __name__ == '__main__':
    uvicorn.run(
        "app:app",
        host='localhost',
        port=8000,
        reload=True
    )

app = FastAPI()


@app.on_event("startup")
async def startup():
    await database.connect()


@app.on_event("shutdown")
async def shutdown():
    await database.disconnect()
