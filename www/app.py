""" API."""

from typing import List

from www.main import app
from www.model.user import User, UserUpdate, UserEntry, UserDelete
from www.model.poll import Poll, PollEntry, PollUpdate, PollDelete
from www.model.question import Question, QuestionEntry, QuestionUpdate, QuestionDelete
from www.model.answer import Answer, AnswerEntry, AnswerUpdate, AnswerDelete
from www.model.result import Result, ResultEntry
from www.crud import user_crud, poll_crud, question_crud, answer_crud, result_crud


# Маршрут пользователя
@app.post("/user", response_model=User)
async def register_user(user: UserEntry):
    return await user_crud.register_user(user)


@app.get("/user/{user_id}", response_model=User)
async def find_user_by_id(user_id: int):
    return await user_crud.find_user_by_id(user_id)


@app.get("/users", response_model=List[User])
async def find_all_users():
    return await user_crud.find_all_users()


@app.put("/users", response_model=User)
async def update_user(user: UserUpdate):
    return await user_crud.update_user(user)


@app.delete("/user/{userId}")
async def delete_user(user: UserDelete):
    return await user_crud.delete_user(user)


# Маршрут опроса
@app.post("/poll", response_model=Poll)
async def create_poll(poll: PollEntry):
    return await poll_crud.create_poll(poll)


@app.get("/poll/{poll_id}", response_model=Poll)
async def find_poll_by_id(poll_id: int):
    return await poll_crud.find_poll_by_id(poll_id)


@app.get("/polls", response_model=List[Poll])
async def find_all_polls():
    return await poll_crud.find_all_polls()


@app.put("/polls", response_model=Poll)
async def update_poll(poll: PollUpdate):
    return await poll_crud.update_poll(poll)


@app.delete("/poll/{poll_id}")
async def delete_poll(poll: PollDelete):
    return await poll_crud.delete_poll(poll)


# Маршрут вопроса
@app.post("/question", response_model=Question)
async def create_question(question: QuestionEntry):
    return await question_crud.create_question(question)


@app.get("/question/{question_id}", response_model=Question)
async def find_question_by_id(question_id: int):
    return await question_crud.find_question_by_id(question_id)


@app.get("/questions", response_model=List[Question])
async def find_all_questions():
    return await question_crud.find_all_questions()


@app.put("/questions", response_model=Question)
async def update_question(question: QuestionUpdate):
    return await question_crud.update_question(question)


@app.delete("/question/{question_id}")
async def delete_question(question: QuestionDelete):
    return await question_crud.delete_question(question)


# Маршрут ответа
@app.post("/answer", response_model=Answer)
async def create_answer(answer: AnswerEntry):
    return await answer_crud.create_answer(answer)


@app.get("/answer/{answer_id}", response_model=Answer)
async def find_answer_by_id(answer_id: int):
    return await answer_crud.find_answer_by_id(answer_id)


@app.get("/answers", response_model=List[Answer])
async def find_all_answers():
    return await answer_crud.find_all_answers()


@app.put("/answers", response_model=Answer)
async def update_answer(answer: AnswerUpdate):
    return await answer_crud.update_answer(answer)


@app.delete("/answer/{answer_id}")
async def delete_answer(answer: AnswerDelete):
    return await answer_crud.delete_answer(answer)


# Маршрут результата
@app.post("/result", response_model=Result)
async def write_result(result: ResultEntry):
    return await result_crud.write_result(result)


@app.get("/result/{result_id}", response_model=Result)
async def find_result_by_id(result_id: int):
    return await result_crud.find_result_by_id(result_id)
