""" CRUD-функции ответа"""


from www.core.db import database, answer_db

__all__ = ["create_answer", "find_answer_by_id", "find_all_answers", "update_answer", "delete_answer"]


async def create_answer(answer):
    query = answer_db.insert().values(
        title=answer.title,
        question_id=answer.question_id
    ).returning(answer_db.c.id, answer_db.c.title, answer_db.c.question_id)

    return await database.fetch_one(query)


async def find_answer_by_id(answer_id):
    query = answer_db.select().where(answer_db.c.id == answer_id)
    return await database.fetch_one(query)


async def find_all_answers():
    query = answer_db.select()
    return await database.fetch_all(query)


async def update_answer(answer):
    query = answer_db.update().where(answer_db.c.id == answer.id).values(
        id=answer.id,
        title=answer.title,
        question_id=answer.question_id
    )
    await database.execute(query)
    return await find_answer_by_id(answer.id)


async def delete_answer(answer):
    query = answer_db.delete().where(answer_db.c.id == answer.id)
    await database.execute(query)
    return {
        "status": True,
        "message": "Deleted this answer successfully."
    }
