""" CRUD-функции результата"""


from www.core.db import database, result_db

__all__ = ["write_result", "find_result_by_id"]


async def write_result(result):
    query = result_db.insert().values(
        question_id=result.question_id,
        answer_id=result.answer_id
    ).returning(result_db.c.id, result_db.c.question_id, result_db.c.answer_id)

    return await database.fetch_one(query)


async def find_result_by_id(result_id):
    query = result_db.select().where(result_db.c.id == result_id)
    return await database.fetch_one(query)
