""" CRUD-функции опроса"""


from www.core.db import database, poll_db

__all__ = ["create_poll", "find_poll_by_id", "find_all_polls", "update_poll", "delete_poll"]


async def create_poll(poll):
    query = poll_db.insert().values(
        name=poll.name,
        type=poll.type,
        disable=poll.disable
    ).returning(poll_db.c.id, poll_db.c.name, poll_db.c.date, poll_db.c.type, poll_db.c.disable)

    return await database.fetch_one(query)


async def find_poll_by_id(poll_id):
    query = poll_db.select().where(poll_db.c.id == poll_id)
    return await database.fetch_one(query)


async def find_all_polls():
    query = poll_db.select()
    return await database.fetch_all(query)


async def update_poll(poll):
    query = poll_db.update().where(poll_db.c.id == poll.id).values(
        id=poll.id,
        name=poll.name,
        type=poll.type,
        disable=poll.disable
    )
    await database.execute(query)
    return await find_poll_by_id(poll.id)


async def delete_poll(poll):
    query = poll_db.delete().where(poll_db.c.id == poll.id)
    await database.execute(query)
    return {
        "status": True,
        "message": "Deleted this poll successfully."
    }
