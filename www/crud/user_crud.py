""" CRUD-функции пользователя"""

from passlib.context import CryptContext

from www.core.db import database, user_db


__all__ = ["register_user", "find_user_by_id", "find_all_users", "update_user", "delete_user"]


pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


async def register_user(user):
    query = user_db.insert().values(
        username=user.username,
        password=pwd_context.hash(user.password),
        first_name=user.first_name,
        last_name=user.last_name,
    ).returning(user_db.c.id, user_db.c.username, user_db.c.password, user_db.c.first_name, user_db.c.last_name,
                user_db.c.create_at)

    return await database.fetch_one(query)


async def find_user_by_id(user_id):
    query = user_db.select().where(user_db.c.id == user_id)
    return await database.fetch_one(query)


async def find_all_users():
    query = user_db.select()
    return await database.fetch_all(query)


async def update_user(user):
    query = user_db.update().where(user_db.c.id == user.id).values(
        first_name=user.first_name,
        last_name=user.last_name,
    )
    await database.execute(query)
    return await find_user_by_id(user.id)


async def delete_user(user):
    query = user_db.delete().where(user_db.c.id == user.id)
    await database.execute(query)
    return {
        "status": True,
        "message": "Deleted this user successfully."
    }
