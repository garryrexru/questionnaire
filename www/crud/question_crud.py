""" CRUD-функции вопроса"""


from www.core.db import database, question_db

__all__ = ["create_question", "find_question_by_id", "find_all_questions", "update_question", "delete_question"]


async def create_question(question):
    query = question_db.insert().values(
        title=question.title,
        poll_id=question.poll_id,
        free_answer=question.free_answer
    ).returning(question_db.c.id, question_db.c.title, question_db.c.poll_id, question_db.c.free_answer)

    return await database.fetch_one(query)


async def find_question_by_id(question_id):
    query = question_db.select().where(question_db.c.id == question_id)
    return await database.fetch_one(query)


async def find_all_questions():
    query = question_db.select()
    return await database.fetch_all(query)


async def update_question(question):
    query = question_db.update().where(question_db.c.id == question.id).values(
        id=question.id,
        title=question.title,
        poll_id=question.poll_id,
        free_answer=question.free_answer
    )
    await database.execute(query)
    return await find_question_by_id(question.id)


async def delete_question(question):
    query = question_db.delete().where(question_db.c.id == question.id)
    await database.execute(query)
    return {
        "status": True,
        "message": "Deleted this question successfully."
    }
