""" Подключение к БД."""


import databases
import sqlalchemy

__all__ = ["database", "user_db", "poll_db", "question_db", "answer_db", "result_db"]


# Подключение к PostgreSQL
DATABASE_URL = "postgresql://postgres:admin@localhost:5432/questionnaire"
database = databases.Database(DATABASE_URL)
metadata = sqlalchemy.MetaData()


# Модели базы данных
# Модель пользователя
user_db = sqlalchemy.Table(
    "user",
    metadata,
    sqlalchemy.Column("id",         sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("username",   sqlalchemy.String),
    sqlalchemy.Column("password",   sqlalchemy.String),
    sqlalchemy.Column("first_name", sqlalchemy.String),
    sqlalchemy.Column("last_name",  sqlalchemy.String),
    sqlalchemy.Column("create_at",  sqlalchemy.DateTime, server_default=sqlalchemy.sql.func.now()),
)


# Модель опроса
poll_db = sqlalchemy.Table(
    "poll",
    metadata,
    sqlalchemy.Column("id",   sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("name", sqlalchemy.String),
    sqlalchemy.Column("date", sqlalchemy.DateTime, server_default=sqlalchemy.sql.func.now()),
    sqlalchemy.Column("type", sqlalchemy.Integer),
    sqlalchemy.Column("disable", sqlalchemy.Boolean, default=False)
)


# Модель вопроса
question_db = sqlalchemy.Table(
    "question",
    metadata,
    sqlalchemy.Column("id",   sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("title", sqlalchemy.String),
    sqlalchemy.Column("poll_id", sqlalchemy.Integer, sqlalchemy.ForeignKey(poll_db.c.id, ondelete='CASCADE')),
    sqlalchemy.Column("free_answer", sqlalchemy.Boolean, default=False)
)


# Модель ответа
answer_db = sqlalchemy.Table(
    "answer",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("title", sqlalchemy.String),
    sqlalchemy.Column("question_id", sqlalchemy.Integer, sqlalchemy.ForeignKey(question_db.c.id, ondelete='CASCADE')),
)


# Модель результата
result_db = sqlalchemy.Table(
    "result",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("question_id", sqlalchemy.Integer, sqlalchemy.ForeignKey(question_db.c.id, ondelete='CASCADE')),
    sqlalchemy.Column("answer_id", sqlalchemy.Integer, sqlalchemy.ForeignKey(answer_db.c.id, ondelete='CASCADE'))
)


engine = sqlalchemy.create_engine(
    DATABASE_URL
)
metadata.create_all(engine)
